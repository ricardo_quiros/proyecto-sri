Instalación y configuración del servidor WEB.
==============================

**Instalación del Servidor Web Apache2.**

Ponemos una de las dos siguientes lineas:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_113.png)

En este caso instalamos apache sobre yast:

    yast -i apache2

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_112.png)

Cuando lo tengamos instalado, iniciamos apache con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_114.png)

El directorio por defecto de los documentos de apache es "/etc/www/htdocs" . Dentro de este directorio creamos un fichero index.html con el contenido "El servidor Funciona!" para probar si funciona nuestro servidor.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_115.png)

En el navegador del equipo ponemos "localhost" o "172.0.0.1". Se ve que sale el contenido de nuestro fichero index.html

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_116.png)

**Configuracion del Servidor:**
*sistemas.informatica.net*
*desarollo.informatica.net*

Primero tenemos que añadir los registros 'A' sistemas y desarrollo en nuestro servidor DNS .

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_117.png)

Desde la maquina w7 probamos si funciona todo. Se ve que sistemas.informatica.net y desarollo.informatica.net corresponden a la IP de nuestro servidor. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_118.png)

Los ficheros de configuración de Apache2 estan en el siguiente directorio:

    /etc/apache2

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_130.png)

Para configurar nuestros servidores virtuales entramos en el directorio /etc/apache2/vhosts.d y creamos dos ficheros de configuración: desarrollo.conf y sistemas.conf

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_161.png)

**Configuración de desarollo.informatica.net**

Así tenemos que configurar el fichero de desarrollo.conf. Hacemos todo paso a paso:

1. DocumentRoot - el directorio raíz de nuestro servidor.
2. ServerName - nombre de dominio de nuestro servidor y al final :443 significa que utilizamos el puerto 443 que es el puerto para HTTPS.
3. El primer directorio es /srv/www/desarollo . Este directorio corresponde a https://desarollo.informatica.net. Podemos listar el contenido del directorio y pueden entrar solo los hosts con IP 172.16.100.4 y 172.16.100.3.
4. El segundo directorio es /srv/www/desarollo/privado. Este directorio corresponde a https://desarollo.informatica.net/privado. La configuración de este directorio esta en otro fichero.
5. Ficheros de log.
6. Configuración de SSL.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_121.png)

*Primero configuramos el servidor para que funcione con SSL.*

- Creamos un certificado digtal con "openssl".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_122.png)

- Luego ponemos el fichero server.key que es nuestra llave privada en el directorio ssl.key y server.crt que es el certificado en el directorio ssl.crt

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_130.png)

- Reiniciamos Apache2 y probamos si funciona.

    systemclt restart apache2.service

En una de las dos maquinas 172.16.100.3 o 4 abrimos un navegador y ponemos:

    https://desarollo.informatica.net


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_123.png)

Este es nuestro certificado que hemos creado antes.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_124.png)

*Segundo configuramos un directorio dentro de nuestro servidor para que pueda entrar solo desarrollo. Esta carpeta es /srv/www/desarollo/privado y corresponde a https://desarollo.informatica.net/privado.*

Tenemos que crear 2 ficheros. Uno de configuración .htaccess y uno para la contraseña .htpasswd dentro del directorio /srv/www/desarollo/privado.

	Primero creamos el fichero .htaccess con el siguiente comando:

    vi .htaccess

Aquí se ve la configuración de este directorio. Esta prohibido el listado del directorio, pueden entrar todos los host pero requiere autenticación del user desarollo.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_167.png)

	Segundo creamos el fichero de la contraseña:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_169.png)

Aquí se ve la contraseña.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_168.png)

Esto es el contenido de nuesto directorio "privado". Un fichero de configuración, fichero con contraseña y un fichero index.html con el contenido: "Esta página es PRIVADA. Solo para usuario desarrollo!!!"

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_166.png)

Prueba de funcionamiento sobre w7. Se ve que requiere autenticación. Ponemos como usuario desarrollo y la contraseña que hemos hecho antes.
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_131.png)

Y ahora podemos entrar en nuestro directorio privado.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_132.png)

* Personalizamos los mensajes de ERROR.*

La configuración de los mensajes de error esta en el siguiente fichero:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_164.png)

En nuestro caso personalizamos el error 403 y 404. Podemos personalizar de dos maneras. Escribimos el texto directamente en el fichero errors.conf o mejor creamos un fichero de html dentro del directorio /usr/share/apache2/error/include.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_129.png)

Dentro de este directorio creamos nuestro fichero de error error404.html con el contenido "La pagina o ruta esta incorrecta!!!!"

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_165.png)

Ahora sobre la maquina con IP 172.16.100.5 que esta prohibida probamos el error 403.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_127.png)

Y aquí con una pagina que no existe probamos el error 404.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_128.png)


**Configuración del Servidir Virtual "sistemas.informatica.net"**


Creamos el fichero sistemas.conf dentro del directorio /etc/apache2/chosts.d.
1. ServerName - el nombre de dominio del servidor(En este caso no es obligatorio que se ponga el puerto :80 porque esta por defecto )
2. DocumentRoot  -  la ruta de la carpeta raíz de nuestro servidor.
3.  Directory - en este caso las configuraciones de la carpeta se hacen sobre otro fichero.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_162.png)

El fichero de configuración del directorio raíz es /srv/www/sistemas/.htaccess
	Ponemos que puedan ver el contenido del directorio y acceso permitido para todos.
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_163.png)

En nuestro caso tenemos que hacer que este fichero de configuración lo pueda modificar el usuario sistemas. Por esto como usuario que puede escribir en la carpeta sistemas ponemos al usuario sistemas.
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SWeb/Seleccio%CC%81n_160.png)











