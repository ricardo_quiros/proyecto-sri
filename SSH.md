Servidor SSH
===========

**Instalación**

Abrimos un terminal como administrador y ponemos lo siguiente:

    zypper in -y sshd

**Iniciación**

Los ficheros de configuración del servidor están en el siguiente directorio:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/e.png)

Arranquamos el servidor con el siguiente comando:

    service sshd start

Probamos el funcionamiento, iniciamos *debian7proyecto* y conectamos por ssh con el usuario root. Se ve que esta conectado correctamente.

 ![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/l.png)

Probamos con otro usuario local: alumno1. Se ve que también se conecta.



![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/n.png)

**Configuración**

Para personalizar el servidor como queremos, tenemos que hacer los siguiente pasos.

Creamos usuarios locales para el servidor ssh. Abrimos yast y seleccionamos*Gestión de ususarios y grupos* .

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/c.png)

Creamos el usuario sshuser.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/b.png)

Creamos el grupo sshlogin y añadimos el usuario sshuser.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/a.png)

Abrimos fichero de configuración con el siguiente comando:

    vi /etc/ssh/sshd_config

Añadimos la siguiente linea, así no permitimos que el usuario root inicie sessión por ssh.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/p.png)

Luego guardamos el fichero y reiniciamos el servidor con el siguiente comando:

    service sshd restart


Podemos ver el estado del servidor con el siguiente comando:

    service sshd status


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/d.png)

Iniciamos debian7proyecto y hacemos una conexión por ssh, donde se ve que la conexión con el usuario root esta prohibida

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/m.png)

Para configurar que puedan conectar estos usuarios que queremos abrimos el fichero del configuración *sshd_conf* y ponemos la siguiente linea:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/h.png)
Con esta linea hemos permitido solo los usuarios que están en el grupo *sshlogin*.
Guardamos el fichero y reiniciamos el servidor.

Para probar el funcionamiento conectamos con el usuario sshuser. Se ve que se conecta correctamente.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/f.png)

Luego probamos con el usuario local de nuestro sistema: alumno1.
Se ve que la conexión con este usuario esta prohibida.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/6edaca6a72ebfd3732f6940ee58b7311b701580a/sshImg/g.png)







