Configuración del servidor DHCP.
===

1 . Para ver si tenemos los paquetes **DHCP** ponemos lo siguiente en un terminal.

    zypper se dhcp

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_077.png)


2 . Para instalar los paquetes que queremos ponemos lo siguiente:

    zypper in dhcp-server yast2-dhcp-server

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_078.png)

3 . Para configurar el **servidor DHCP** utilizamos **YaST** , donde seleccionamos *Dispositivos de red* y luego *Servidor DHCP*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_095.png)

4 . Primero seleccionamos la tarjeta de red que utilizaremos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_096.png)

5 . Luego introducimos la **Configuración global**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_097.png)

6 . En el menú **DHCP dinámico** se pone el rango de *IP'S* y *Tiempo de asignación*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_098.png)

7 . En el menú **Administración de host** ponemos las reservas de host mirando primero la **MAC** de los host que queremos reservar y luego asignamos la **IP** que queremos con cada **MAC**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_111.png)

8 . Cuando tenemos todo configurado en el menú **Inicio** seleccionamos *Guardar la configuración y reiniciar el servidor DHCP ahora*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_100.png)

9 . Para verificar si el servidor funciona ponemos en un *Terminal* lo siguiente:

    netstat -aun

Si esta abriendo el puerto *67/UDP*  el **servidor DHCP** funciona. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_101.png)

10 . Para consultar el fichero de configuración de **DHCP** ponemos lo siguiente en un *Terminal*.

    vi /etc/dhcpd.conf

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_110.png)

11 . Para probar si funcionan las reservas de **IP'S** arrancamos primero el **Windows 7** que tiene la **MAC** 08:00:27:87:6c:c3 y lo configuramos por **DHCP**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_103.png)

Seleccionamos **cmd**.

    ipconfig/all

Se ve que la **MAC** del host esta configurada con la **IP** correcta.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_109.png)

12 . La segunda reserva la probamos arrancando un host **debian** que tiene la **MAC** 08:00:27:32:f0:d3 y lo configuramos por **DHCP**.  Luego reiniciamos la tarjeta de red con los siguientes comandos:

    /etc/init.d/networking stop

    /etc/init.d/networking start

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_108.png)

Luego ponemos en el **Terminal**.

    ifconfig

Se ve que la **MAC** del host esta configurada con la **IP** correcta.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_107.png)

Con esto tenemos el **servidor DHCP** configurado y funcionando.