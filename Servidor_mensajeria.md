
Servidor de mensajería
===================

Para el servidor de mensajería utilizamos *OpenFire*. Para instalarlo tenemos que añadir otro repositorio. Lo hacemos con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_001.png)

Ahora podemos instalar *OpenFire*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_002.png)

Cuando lo instalamos tenemos que iniciarlo con el siguiente comando: 
 
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_014.png)

Para ver si todo esta bien escribimos *netstat -lnt* y  observamos el puerto en el que escucha *OpenFire*, 9090.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_003.png)

Entramos en la página de configuración poniendo en el navegador *localhost:9090*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_015.png)

Seleccionamos el idioma que queremos

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_016.png)

Luego como domino ponemos *informatica.net* y continuamos

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_017.png)

Seleccionamos Conexión Estándard.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_018.png)

Ahora tenemos que crear una base de datos dentro de nuestro servidor. Entramos en phpMyAdmin y creamos la base de datos *openfire*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_010.png)

Como usuario podemos utilizar el siguiente:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_011.png)

Ahora podemos continuar con la configuración de *OpenFire*. En *Drivers Predifinidos* seleccionamos MySql, *Clase del Driver JDBC* lo dejamos por defecto, *URL de la Base de Datos* tenemos que poner la ip de nuestro servidor de base de datos y luego el nombre de la base de datos que hemos creado, y para acabar ponemos el usuario *root* y la contraseña que tenemos para este usuario. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_019.png)

Seteos de Perfil lo dejamos por defecto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_020.png)

Ya hemos configurado nuestro *OpenFire*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_021.png)

Para entrar en *OpenFire* podemos utilizar el usuario *admin* con contraseña *admin*  creados por defecto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_022.png)

Ahora tenemos que crear usuarios que pueden utilizar nuestro servidor de mensajería. Pinchamos en *Usuarios/Grupos*. Se ve que hay solo un usuario *admin*. Para crear nuevos usuarios pinchamos en *Crear Nuevo Usuario*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_023.png)

Creamos el usuario *filemon*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_024.png)

Creamos el usuario *mortadelo*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_025.png)

Ahora tenemos tres usuarion en nuestro *OpenFire*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_026.png)

**Cliente de mensajeria: Spark** 

Instalamos *Spark* en un windows 7. Para conectarnos utilizamos el usuario *mortadelo*. En servidor ponemos la IP de nuestro servidor *OpenFire* y pinchamos en Login.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_004.png)

Se ve que el usuario *mortadelo* esta conectado.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_005.png)

Ahora instalamos *Spark* en un ubuntu. Para conectarnos utilizamos el usuario *filemon* . En servidor ponemos la IP de nuestro servidor *OpenFire* y pinchamos en Login.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_006.png)

Se ve que el usuario *filemon* esta conectado.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_007.png)

Para buscar otros usuarios de *OpenFire* que están conectados, más abajo ponemos el nombre del usuario que queremos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_027.png)

Cuando lo tenemos , lo agregamos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_028.png)

Ahora para probar el funcionamiento, enviamos un mensaje desde *mortadelo* a *filemon*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_008.png)

Se ve que *filemon* ha recibido el mensaje de *mortadelo*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_009.png)