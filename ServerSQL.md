Servidor MySql
============



Instalamos el servidor MySql con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SMySql/Seleccio%CC%81n_135.png)

MariaDB es la aplicación que utilizamos para el servidor SQL.
Aquí empieza la instalación con yast.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SMySql/Seleccio%CC%81n_136.png)


Cuando esta instalado tenemos que configurar la contraseña para el usuario root. Se hace con el siguiente comando:

    mysqladmin -u root password

-u es para el usuario

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SMySql/Seleccio%CC%81n_139.png)


Para comprobar que funciona nuestro servidor mysql, ponemos el siguiente comando:

    mysql -u root -p

Se ve que entramos en MariaDB sin problemas.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_SMySql/Seleccio%CC%81n_140.png)