BitBucket
========
Utilizamos este sistema de control de versiones. Accedemos con la cuenta que tenemos de google y creamos un nuevo repositorio donde se tendrá el control de todos los ficheros relacionados con el proyecto.

![enter image description here](https://bitbucket.org/ricardo_quiros/proyecto-sri/raw/4922d653dd4fd0a659ce16a6a0b00225955ecb08/MemoriaImg/Captura%20de%20pantalla%202015-03-16%20a%20las%201.14.31.png)

BitBucket nos facilita una herramienta llamada **SourceTree** para desde ella controlar el repositorio creado. Esta herramienta crea al instalarse una carpeta en el `home` del usuario que la instala con el nombre del repositorio donde se almacenaran todos los documentos. Cada vez que nos conectemos a nuestro repositorio con *SourceTree* buscará cambios en dicha carpeta y en la carpeta de los usuarios autorizados para trabajar con nuestro repositorio.

![enter image description here](https://bitbucket.org/ricardo_quiros/proyecto-sri/raw/4922d653dd4fd0a659ce16a6a0b00225955ecb08/MemoriaImg/Captura%20de%20pantalla%202015-03-16%20a%20las%201.23.44.png)

MARKDOWN
==========
Como editor de este lenguaje utilizamos:

> [StackEdit](https://stackedit.io/).

Esta web nos permite crear e importar/exportar documentos, también sincronizarlos en tiempo real con google drive para no perder nuestros avances y una vez finalizado el documento publicarlo en blogger.  

![enter image description here](https://bitbucket.org/ricardo_quiros/proyecto-sri/raw/4922d653dd4fd0a659ce16a6a0b00225955ecb08/MemoriaImg/Captura%20de%20pantalla%202015-03-16%20a%20las%201.04.56.png)

BLOGGER
========

Usando una cuenta de google utilizamos esta herramienta para a través de nuevas entradas ir publicando nuestros avances. 

![enter image description here](https://bitbucket.org/ricardo_quiros/proyecto-sri/raw/4922d653dd4fd0a659ce16a6a0b00225955ecb08/MemoriaImg/Captura%20de%20pantalla%202015-03-16%20a%20las%201.06.57.png)

Podremos personalizar nuestro blog con la plantilla que queramos. 

![enter image description here](https://bitbucket.org/ricardo_quiros/proyecto-sri/raw/4922d653dd4fd0a659ce16a6a0b00225955ecb08/MemoriaImg/Captura%20de%20pantalla%202015-03-16%20a%20las%201.07.34.png)



