Configuración del Servidor DNS Dinámico. DynDNS
========================

1 . Primero abrimos YaST y luego "Servidor DNS".  Para que funcione el servidor DNS Dinámico tenemos que configurar una clave TSIG. En el menú principal seleccionamos "Claves TSIG". Ahora tenemos varios pasos para crearla.

 - En "Crear una nueva clave TSIG". Ponemos un ID de clave cualquiera y el nombre del archivo, en nuestro caso "dyndns.key" y seleccionamos "Generar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_029.png)

 

 - Seleccionamos "Si" para continuar.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_030.png)

 - Ahora se vee que esta creada una clave "dyndns.key" que esta en el directorio /etc/named.d

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_031.png)

 - Para ver si esta creada la clave ponemos el siguiente comando:

    ls /etc/named.d

Se vee que ha creado un fichero con la clave publica, un fichero con la clave privada y el fichero dyndns.key.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_037.png)

2 . El segundo paso es crear zonas para nuestro DNS Dinámico en varios pasos.

 - Creamos zona maestra para el subdominio "dinamico.informatica.net" y pinchamos en "Editar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_032.png)

 - En "Registros NS" ponemos  el nombre de nuestro servidor, la zona: "suse.dinamico.informatica.net." y "Añadir".




![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_033.png)

 - Seleccionamos "Fundamentos" y activamos "Permitir actualizaciones dinámicas y seleccionamos  el ID de nuestra clave "dyndns".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_034.png)

 - Para hacer una zona inversa tenemos que utilizar la misma zona informatica.net, pero cuando la configuremos se pierden las configuraciones que hemos creado o se crea una nueva zona pero con máscara 16 que practicamente funciona igual como la de máscara 24 en este caso. En nuestro caso para no perder las configuraciones creamos una nueva zona inversa "16.172.in-addr.arpa" como maestra y pinchamos "Editar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_051.png)

 - En "Registros NS" ponemos el mismo que en la zona directa: "suse.informatica.net."

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_054.png)

 - En "Fundamentos" activamos "Permitir actualizaciones dinámicas" y más abajo activamos "Generar automáticamente los registros del zona" y seleccionamos la zona de DNS Dinámico. Con esta opción el fichero de zona inversa se actualiza automáticamente cuando se carga un nuevo registro en la zona directa.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_036.png)

 - Consultamos el fichero de configuracion de bind con el siguiente comando:

    cat /etc/named.conf

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_057.png)

 - Se vee que estan creadas dos zonas mas que tienen la opción de "allow-update". Esto permite coger actualizaciones dinámicas.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_056.png)


 - Cuando aceptamos la opción "Permitir actualizaciones dinámicas" los ficheros se han credo en el directorio /var/lib/named/dyn no en el directorio /var/lib/named/master como la zona DNS estática.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_055.png)

 - Para configurar la zona "dinamico.informatica.net" como subdominio de la zona "informatica.net" tenemos que configurar la zona "dinamico.informatica.net" de la siguiente manera:

Abrimos el fichero: 

    vi /etc/lib/named/dyn/dinamico.informatica.net

Ponemos dos registros "A"
dinamico.informatica.net.     IN    A     172.16.100.100
suse										 									         IN	 A		172.16.100.100

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_060.png)

 - Tenemos que añadir un registro en la zona "informatica.net.", 
 abrimos el fichero: 

    vi /etc/lib/named/master/informatica.netXX

Ponemos un registro NS

dinamico.informatica.net		IN		NS		suse.dinamico.informatica.net.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_059.png)

3 . Ahora desde YaST abrimos "Servidor DHCP". Para configurar y utilizar "DNS Dinamico"  hacemos los siguientes pasos:

 - Seleccionamos "Configuración avanzada" y "Si".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_038.png)

 - Seleccionamos "subnet....." y "Editar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_039.png)

 - Seleccionamos el botón "DNS dinamico".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_040.png)

 - Aquí en "Nombre de archivo" buscamos y seleccionamos el archivo "dyndns.key" que hemos creado antes y seleccionamos "Añadir".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_041.png)

 - Se vee que el fichero esta añadido. Pinchamos en "OK".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_043.png)

 - Aquí seleccionamos "Activar DNS dinámico para esta subred". Luego para clave de zona directa e inversa seleccionamos el ID de nuestra clave "dyndns". Seleccionamos "Actualizar configuracion global" (se explica luego la razón). En Zona ponemos la zona que creada para dinámico e IP del servidor. Luego en zona inversa ponemos la zona inversa que hemos creado e IP del servidor. Al final seleccionamos "OK".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_052.png)

 - Seleccionamos "Terminar" para gruardar configuraciones.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_045.png)

 - Abrimos otra vez las configuraciones avanzadas del Servidor DHCP. Pinchamos en "Opciones globales" y "Editar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_046.png)

 - Aquí se veen las reglas que estan creadas cuando hemos seleccionado antes "Actualizar configuracion global". Se han creado "ddns-update-style" y "ddns-updates". Si no están creadas tenemos que crearlas manualmente. Otra cosa que tenemos hacer es modificar el "optional-domain-name" con  el nombre de nuestro dominio dinámico. Cuando todo esta configurado pinchamos "OK", luego " Terminar" y con esto todo esta configurado.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_053.png)

4 . Para probar si todo funciona correctamente necesitamos un host con windows y uno con linux distintos de los que estan configurados en la zona "informatica.net.". Por esto arrancamos el host w701 y debian01.

 - Reiniciamos el servidor DNS con el siguiente comando:

    rcnamed restart

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_062.png)

 - Para verificar si todo esta corecto consultamos el log de el siguiente fichero:

    tailf /var/log/masseges

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_061.png)

 - Se vee que todo esta arrancado sin errores.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_063.png)

 - Prueba con Windows 7

Arrancamos el host, ponemos en el menú de inicio "cmd", luego escribimos los siguientes dos comandos para coger nuevas configuraciones.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_064.png)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_065.png)

En la siguiente captura se vee que el servidor DHCP ha configurado con la correcta IP y nombre de dominio.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_067.png)

Hacemos una consulta a el fichero masseges de nuestro servidor y se vee que esta creado un DNS dinámico.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_068.png)

Con el siguiente comando consultamos el fichero de la zona directa de nuestro Servidor DynDNS.

    cat /var/lib/named/dyn/dinamico.informatica.net

Se vee que ha creado un registro A de w701 con ip 172.16.100.8 y un registro de tipo texto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_069.png)

Consultamos la zona inversa:

    cat /var/lib/named/dyn/16.172.in-addr.arpa

Se vee que esta creado un registro de tipo PTR de w701.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_070.png)

 - Prueba con Debian.

Reiniciamos servicios de red:

    /etc/init.d/networking restart

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_071.png)

Se vee que el Servidor DHCP esta configurado correctamente en el host debian01.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_072.png)

Consumtamos el fichero massege de nuestro servidor. Se vee que esta creado un DNS dinamico.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_073.png)

Con el siguiente comando consultamos el fichero de la zona directa de nuestro Servidor DynDNS.

    cat /var/lib/named/dyn/dinamico.informatica.net

Se vee que ha creado otro registro A de debian01 con ip 172.16.100.9 y un registro de tipo texto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_079.png)

Consultamos la zona inversa:

    cat /var/lib/named/dyn/16.172.in-addr.arpa

Se vee que ha creado un registro de tipo PTR de debian01.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_078.png)

 - Prueba general con nslookup con w701

zona dinamico.informnatica.net.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_074.png)

zona informatica.net.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_075.png)

 - Prueba general con nslookup con debian01

zona dinamico.informatica.net.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_076.png)

zona informatica.net.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8293fdae8808f7ef6f166f908e3ea0e41c376b7d/DynDNS/Selecci%C3%B3n_077.png)


