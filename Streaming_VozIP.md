Instalación y configuración de Servidores de Streaming y VozIP.
============================

Para los servidores vamos a utilizar *Ubuntu12.04*. La máquina la llamamos *Jupiter*. La configuramos por DHCP. Queremos utilizar la IP 172.16.100.201, entonces hacemos una reserva en nuestro servidor DHCP para esta máquina:


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_029.png)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_030.png)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_031.png)

Queremos conectar esta máquina sobre *jupiter.informatica.net*. Entonces creamos un registro A dentro de nuestro servidor DNS.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_033.png)


**Servidor de Streaming *icecast2* **
---------------------------------------

Primero instalamos *icecast2* con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_038.png)

Los ficheros de configuración de *icecast2* están dentro del directorio /etc/icecast2. Para configurarlo abrimos el fichero **icecast.html**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_039.png)

Dentro de este fichero tenemos que modificar varias líneas:

 - Tenemos que poner el nombre de nuestro servidor y el puerto en el que escuchará.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_040.png)

 - Configuramos el usuario y contraseña.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_041.png)

 - Tenemos que establecer los límites según nuestras necesidades.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_042.png)

Ahora podemos conectarnos a *icecast2* con *jupiter.informatica.net:8000*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_034.png)

Tenemos que instalar dos paquetes más:

 - *ffmpeg2theora* Convierte el video al formato ogg.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_043.png)

 - *oggfwd* Inyecta el video dentro de *icecast*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_044.png)

Cuando tenemos todo instalado, entramos en una carpeta en la que tengamos videos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_032.png)

Ahora para empezar el streaming ponemos lo siguiente:

    ffmpeg2theora Prueba.3gp -o /dev/stdout | oggfwd jupiter.informatica.net 8000 admin /prueba.ogv

Donde Prueba.3gp es nuestro video,  jupiter.informatica.net es el dominio de nuestro servidor, 8000 el puerto que escucha, admin el usuario que hemos configurado antes en *icecast2* y /prueba.ogv el nombre de salida. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_036.png)

Si entramos en jupiter.informatica.net:8000 se ven las opciones de nuestro video.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_035.png)

Para ver el video ponemos lo siguiente en nuestro navegador:

    http://jupiter.informatica.net:8000/prueba.ogv

Observamos que nuestro servidor Streaming funciona.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_037.png)


**Servidor VozIP Asterisk**
-----------------------------


Para instalar *Asterisk* ponemos el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_062.png)

El directorio de *Asterisk* es /etc/asterisk. Dentro de este directorio hay varios ficheros. Nosotros tenemos que tocar *sip.conf* y *extensions.conf*

Primero modificamos el fichero *sip.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_046.png)

Dejamos la directiva general. Tenemos que poner los números que habrá. Ponemos el numero 100 con contraseña 123 y el número 101 con contraseña 456.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_045.png)

Ahora para configurar nuestros números tenemos que tocar el fichero *extensions.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_048.png)

Para los dos números ponemos extensiones para coger, llamar, que haya música cuando esperan y que puedan tener conexión.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_047.png)

Cuando tenemos todo configurado podemos arrancar la consola del *Asterisk* con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_058.png)


Para hacer una prueba utilizamos dos Windows 7 con una aplicación VozIP que se llama *Jabber*.

Ahora ponemos la misma configuración para todos los números que tenemos:

Primero tenemos que crear nuevos perfiles. Abrimos Jabber y seleccionamos *Options*, *Profiles* y *New*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_049.png)

Ahora creamos el perfil *neo*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_050.png)

Pinchamos en *SIP Proxy* y ponemos el dominio de nuestro servidor, que es *jupiter.informatica.net*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_051.png)

Sale una ventana donde tenemos que poner el número y la contraseña que hemos configurado antes en el fichero *sip.conf*. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_052.png)

Hacemos lo mismo pero en otro windows 7 pero como perfil morfeo y número 101.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_054.png)

Cuando tenemos todo configurado en el perfil neo podemos agregar el numero 101 con el nombre que queramos. Esto mismo podemos hacerlo en el perfil morfeo.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_053.png)

Si todo esta bien hecho podemos hacer una llamada de morfeo a neo.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_055.png)

En el otro window 7 con el perfil neo observamos que morfeo esta llamando.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_056.png)

En la consola de *Asterisk* si ponemos *sip show channels* se ve que esta llamando.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_059.png)

Cuando neo pincha *Answer*  se inicia la conversación

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_057.png)

En la consola de *Asterisk* si ponemos *sip show channels* se ve que hay una conexión entre *100* y *101*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20Proyecto3/Seleccio%CC%81n_060.png)




