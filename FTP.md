Servidor FTP
===========

**Instalación:**

Abrimos un terminal como administrador y ponemos el siguiente comando:

      zypper in -y vsftpd

**Configuración**


Primero creamos dos usuarios locales que tendrán todos los privilegios. Los usuarios son *desarrollo* y *sistemas* que pertenecen al grupo ftp-users. También creamos el directorio de trabajo del servidor FTP.

 -Creamos el directorio:

    mkdir /home/autent

 -Creamos el grupo de usuarios:

    groupadd ftp-users

 -Creamos los usuarios desarrollo y sistemas que pertenecen al grupo *ftp-users* y con directorio /home/autent:

    useradd -g ftp-users -d /home/autent desarollo
    passwd desarollo

    useradd -g ftp-users -d /home/autent sistemas
    passwd sistemas

 -Ponemos privilegios a /home/autent para el grupo ftp-users:

    chmod 470 /home/autent

 -Asignamos que el directorio /home/autent pertenezca al grupo ftp-users:

    chown root:ftp-users /home/autent

 -Configuramos el fichero de configuración de vsftpd:

    vi /etc/vsftpd.conf

 ![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/29.png)

Con este fichero configuramos todas las opciones del servidor. Se configuran usuarios anónimos y locales, los directorios que se utilizan y controlan los recursos del servidor.
	En la sección de local users esta la linea *chroot_list_file=/etc/vsftpd.chroot.list* . Por defecto este fichero no esta creado. Tenemos que crearlo:

    touch /etc/vsftpd.chroot.list

Luego lo abrimos y escribimos dentro los usuarios locales que queremos que puedan utilizar nuestro servidor FTP.

    vi /etc/vsftpd.chroot.list

Y dentro escribimos *desarrollo* y *sistemas* y guardamos.

 -Al final reiniciamos el servidor:

    rcvsftpd restart

 -Podemos ver el estado de nuestro servidor con el siguiente comando:

    rcvsftpd status

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/57315be31ea016a4eb8497229c24452cc74e435a/ftp/30.png)

 -Ahora tenemos que configurar los cortafuegos del sistema y añadiendo una regla que abre el puerto 21.

Abrimos Yast y seleccionamos el cortafuegos.
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/57315be31ea016a4eb8497229c24452cc74e435a/ftp/32.png)

A la izquierda seleccionamos *servicios autorizados*, luego zona externa, seleccionamos *Servidor vsftpd* y pinchamos *Añadir* .
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/57315be31ea016a4eb8497229c24452cc74e435a/ftp/33.png)


 -Ponemos el siguiente comando a ver si nuestro sistema escucha en el puerto 21 TCP:


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/57315be31ea016a4eb8497229c24452cc74e435a/ftp/31.png)



**Prueba del funcionamiento**

 -Prueba de los usuarios anonymous.


Con esta imagen se ve que que hemos conectado con el servidor FTP. Antes hemos creado 3 ficheros para la prueba.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/1.png)

Cuando probamos a subir un archivo se ve que esta prohibido.

![](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/2.png)

Aquí se ve que podemos descargar archivos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/4.png)

 -Prueba de los usuarios locales.


En esta imagen se ve que hemos conectado correctamente con el servidor.
Antes hemos creado 3 ficheros para la prueba.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/5.png)

Aquí se ve que podemos subir archivos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/7.png)


Aquí se ve que podemos borrar y descargar archivos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/8.png)


Aquí hemos conectado con el usuario sistemas, se ve que podemos subir, descargar y borrar archivos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/954d9ed5ec42205a0076fdaa2aa7516fa0b901c4/ftp/9.png)