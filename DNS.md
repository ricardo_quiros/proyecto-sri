Configuración del servidor DNS.
=========================

1 . Abrimos un terminal con privilegios de administrador e instalamos bind con el siguiente comando.

    zypper in bind

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_001.png)


2 . Entramos en YaST.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_002.png)

3 . A la izquierda seleccionamos "Dispositivos de red" y luego a la derecha "Servidor DNS"

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_003.png)

4 . Para configurar el servidor para que reenvie las consultas para las que no esta autorizado hacemos lo siguiente:
	 En el menu pinchamos en Redireccionadores, luego en " Añadir dirección IP" ponemos la ip del servidor que queremos utilizar y pinchamos "Añadir".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_004.png)

5 . Ahora creamos las zonas del servidor.
	En "Nombre" ponemos el nombre de nuestra zona y luego pinchamos "Añadir", poniendo la zona directa "informatica.net y la zona inversa "100.16.172.in-addr.arpa". La zona es para la red 172.16.100.0/24.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_005.png)

6 . Selecionamos la zona "informatica.net" y pinchamos "Editar". Ahora sale el menu de configuracion de esta zona. Pinchamos en "Registros(Records) NS" para crear un registro de servidor de nombres. Nuestro servidor se llama SUSE, entonces llamamos al registro suse.informatica.net. .  

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_006.png)

7 . Si quieremos configurar un dns para e-mail selecionamos "Registros MX". En nuestro caso lo pasamos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_007.png)

8 . "SOA" es el registro de configuracion del zona. Aqui se configura serie, time to live-TTL y otras propiedades. En este caso lo dejamos con las configuraciones por defecto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_008.png)

9 . Pinchamos en "Registros" para añadir los registros de los hosts. También desde aquí se puede añadir otros registros como "CNAME" , "MX" , etc. Primero y muy importante hay que definir la IP de nuestro servidor. Entonces en "Clave del registro" ponemos el nombre de nuestro servidor "suse" (se puede poner suse.informatica.net. pero cuando nuestra zona es "informatica.net." se escribe automaticamente),  en Tipo seleccionamos "A", luego ponemos la IP y pinchamos en "Añadir". Es lo mismo para otros registros que queramos añadir para distintos hosts. Cuando terminamos con la zona directa seleccionamos continuar.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_009.png)

10 . El siguiente paso es configurar la zona inversa. Seleccionamos la zona y pinchamos "Editar".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_010.png)

11 . En "Registros (Records)NS" ponemos el mismo como en la zona directa "suse.informatica.net.". 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_011.png)

12 . Las propiedades del registro las dejamos por defecto.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_012.png)

13 . Los registros de la zona inversa son "PTR" y en "Clave de registro" se pone la ip del host. En "Valor" se pone el nombre del registro "A" que hemos creado en la zona directa. En clave también se puede escribir solo el ultimo numero de la ip del host que queremos o se escribe completo "2.100.16.172.in-addr.arpa.".Cuando terminamos con la zona inversa seleccionamos continuar.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_013.png)

14 . Ahora tenemos todo configurado del servidor DNS. Pinchamos en "Inicio" y luego "Guardar ajustes y volver a cargar el servidor DNS ahora". Después pinchamos en "Terminar" para salir de YaST.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_014.png)

15 . Para revisar el fichero del configuración del servidor en un terminal ponemos lo siguiente: 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_015.png)

16 . Se vee que hemos creado 2 zonas de tipo "master" que están relacionadas con los ficheros de la carpeta master.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_016.png)

17 . Para ver si están creados los fichero ponemos lo siguiente, donde se vee que están creados.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_017.png)

18 . Para ver el contenido del fichero de zona directa ponemos el siguiente comando:

    vi /etc/lib/named/master/informatica.netXX

Se abre el fichero y se vee la configuracion del registro SOA, abajo esta el registro del servidor NS, y al final hay cuatro registros "A" de los hosts.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_018.png)

19 . Para ver el contenido del fichero de zona inversa ponemos lo siguiente:

    vi /etc/lib/named/master/100.16.172.in-addr.arpaX

Aquí también por arriba aparece "SOA" , luego el registro del servidor y al final los registros "PTR" de los hosts.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_019.png)

20 . Al final tenemos que hacer una modificación del servidor DHCP que tenemos. Abrimos YaST y seleccionamos "Servidor DHCP".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_021.png)

21 . Pinchamos en "Configuracion global". En "Nombre de dominio" ponemos *informatica.net* y en "IP del servidor de nombres privadas" ponemos la IP de nuestro servidor *172.16.100.100*. Asi el servidor DHCP envia esta configuracion a todos los equipos de nuestra red.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_020.png)

22 . Para probar si funciona todo correcto arrancamos la maquina debianproyecto, reiniciamos el servicio de red con el siguiente comando: 

/etc/init.d/networking restart

Ponemos el comando "nslookup" , luego para ver el servidor por defecto ponemos "server". Se vee que el servidor es "172.16.100.100".
Luego escribimos el nombre del host que queremos ver. En nuestro caso podemos preguntar con el nombre del host solo "w7proyecto" o con el nombre completo "w7proyecto.informatica.net." por que hemos configurado el servidor DHCP que envía el nombre de domino por defecto "informatica.net".

Esta captura es de una prueba de la zona "directa".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_022.png)

23 . Esta captura es de una prueba de la zona "inversa".

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_023.png)

24 . Con esta captura probamos si funciona el redireccionamiento que hemos configurado antes para dominios que no están autorizados en nuestro servidor DNS.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_024.png)

25 . Al final hacemos una prueba con w7proyecto. Reiniciamos las configuraciones con los siguientes comandos:

    ipconfig/release
    ipconfig/renew
En esta captura se vee que ha cogido las nuevas configuraciones.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_026.png)

26 . Hacemos una prueba de la zona directa.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_027.png)

27 . Hacemos una prueba de la zona inversa.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_028.png)


***Ya tenemos un servidor DNS configurado!!!***

