
Aplicaciones web: PHPMYADMIN y WORDPRESS
=============================

**Instalación y configuración de PhpMyAdmin**


Primerio tenemos que instalar php, php-mysql y un modulo de php para apache2.
![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_141.png)

Se pueden instalar con uno de los dos comandos:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_142.png)

Luego activamos el modulo php5 en apache2 con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_143.png)

Ahora tenemos que instalar phpMyAdmin

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_144.png)


Se puede instalar con uno de los dos comandos:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_145.png)

Después de tener todo instalado podemos probar el funcionamiento de phpMyAdmin. Por defecto se abre desde localhost o 172.0.0.1. Para entrar en phpMyAdmin ponemos como ususario root y contraseña que hemos configurado en el MySql.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_146.png)

Se ve que phpMyAdmin funciona correctamente.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_147.png)

Tenemos que hacer accesible el phpMyAdmin desde nuestro servidor sistemas.informatica.net/phpmyadmin. La carpeta  del phpMyAdmin esta en /srv/www/htdocs y tenemos que hacer un enlace simbólico dentro de la carpeta de nuestro servidor.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_170.png)

Primero tenemos que configurar phpMyAdmin para que soporte Alias. El fichero de configuración esta dentro de /etc/apache2/conf.d. El fichero se llama phpMyAdmin.conf

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_171.png)

Tenemos que poner Options FollowSymLinks y configuramos un Alias.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_172.png)

Al final dentro del directorio de nuestro servidor /srv/www/htdocs creamos un enlace simbólico para la carpeta de phpMyAdmin con el siguiente comando:

    ln -s /srv/www/htdocs/phpMyAdmin phpmyadmin

En la siguiente imagen se ve que hemos creado el enlace simbólico phpmyadmin.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_173.png)

Probamos su funcionamiento sobre w7. Se ve que phpMyAdmin funciona desde nuestro servidor.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_149.png)



**Instalación y configuración de wordpress**


Primero tenemos que descargar wordpress desde pagina oficial. Luego descomprimimos el fichero con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_154.png)

Luego copiamos la carpeta wordpress en el directorio /srv/www/htdocs

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_175.png)

Ahora tenemos que crear un usuario para wordpress sobre phpMyAdmin.
Seleccionamos 'Bases de datos'.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_150.png)

Luego creamos una base de datos nueva con el nombre wordpress

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_151.png)

Creamos un usuario dentro de la base de datos wordpress. Como nombre de ususario ponemos wordpress, servidor localhost y una contraseña.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_152.png)

Ahora entramos en el directorio de wordpess /srv/www/htdocs/wordpress. Dentro de este directorio hay un fichero wp-config-sample.php. Copiamos este fichero y lo renombramos a wp-config.php

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_155.png)

Luego abrimos este fichero con el siguiente comando: 

    vi wp-config-php


Y escribimos el nombre de la base de datos, nombre del usuario y contraseña que hemos creado antes en phpMyAdmin.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_176.png)

Falta solo que configuremos wordpress para que se arranque sobre "sistemas.informatica.net/wordpress". Tenemos que configurar el fichero de nuestro servidor sistemas.informatica.net. Agregamos un nuevo 'directory' que este configurado para seguir enlaces simbólicos. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_158.png)

Al final creamos un enlace simbólico para wordpress dentro del directorio raíz de nuestro servidor con el siguiente comando:  

    ln -s /srv/www/htdocs/wordpress wordpress

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_174.png)

Ahora probamos el funcionamiento de wordpress. Sobre w7 ponemos "sistemas.informatica.net/wordpress". Se ve que funciona correctamente. Como usuario y contraseña ponemos el que hemos configurado antes en phpMyAdmin y dentro del fichero wp-config.php.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_156.png)

Aquí se ve que nuestro servidor esta listo.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/03c53c297fc60b64c833e08e8255b26a0ab07892/IMG_AplicacionesWeb/Seleccio%CC%81n_157.png)













