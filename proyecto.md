**PROYECTO SERVICIOS DE RED E INTERNET**
============================           

- Ricardo Quirós
- Marin Nikolov

##Resumen:

[TOC]

ESQUEMA DE RED
=================

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/8fad2326f8d0ce96abdb646cb8c45c15a6199f2b/DNSimg/Selecci%C3%B3n_050.png)


Instalación y configuración  del router
=============================


##*Requisitos*

- Imagen de IpFire
- Procesador - 333Mhz o mejor
- Memoria RAM - 256Mb o más
- Disco Duro - 2Gb
- Red - 2 tarjetas con 100Mbit/1Gbit 


Instalación
------------

1 . Montamos la imagen de IpFire y ponemos:
> Install IPFire 2.15 - Core 83

![](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_017.png)

2 . Seleccionamos **lenguage**

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_018.png)

3 . Seleccionamos **OK**

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_019.png)

4 . Aceptamos la licencia.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_020.png)

5 . Seleccionamos ext4 como sistema de ficheros y esperamos hasta que se instale el sistema.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_021.png)

6 . Después de la instalación reiniciamos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_022.png)

>***Con esto termina la instalación de el router con IPFire.***

Configuración
------------------

1 . Seleccionamos **IPFire**


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_023.png)

2 . Selecionamos tipo de teclado, **es** para español.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_024.png)

3 . Seleccionamos la zona de tiempo.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_025.png)

4 . Introducimos el nombre del router o host.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_026.png)

5 . Introducimos el nombre de dominio.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_027.png)

6 . Ponemos la contraseña **root**  para acceder a la linea de comandos.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_028.png)

7 . Ponemos la contraseña **admin**  para acceder a la página de administración del router.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_029.png)

8 . En el siguiente menú seleccionamos:
>Tipo de configuración de red

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_030.png)

9 . En este caso seleccionamos **GREEN+RED**, donde **GREEN** es la tarjeta de red para la red local y **RED** es la tarjeta de red para internet o conexión con otras redes.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_031.png)

10 . Seleccionamos siguiente para asignar que tarjeta para la red local **GREEN**  y cual para la red exterior **RED**.
>Drivers y asignación de tarjeta

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/83536ecb78eb2c8e64d196e4fbdee485da103a14/proyecto%20img/Selecci%C3%B3n_112.png)

11 . Seleccionamos **GREEN** y la tarjeta que queremos para red local.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_032.png)

12 . Seleccionamos **RED** y la otra tarjeta.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_033.png)

13 . Cuando tenemos todo asignado seleccionamos **Terminado**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_034.png)

14 . Para configurar *IP'S* de las tarjetas seleccionamos:
>Configuración de direcciones

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_035.png)

15 . Seleccionamos la interfaz **GREEN** o **RED** para poner *IP* y *mascara* de la red.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_036.png) 

16 . En la interfaz **GREEN** ponemos la *IP* y la *máscara* de nuestra red local.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_037.png)

17 . En la interfaz **RED** ponemos la *IP* y *máscara* de la red externa. Tenemos 3 opciones donde la primera es para poner configuraciones *estáticas*, la 2 es *DHCP* que coge configuraciones automáticas y la 3 es *PPP DIALUP*. En nuestro caso utilizamos configuración estática donde sabemos que la red externa es 192.168.1.0 con máscara /24.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_038.png)

18 . Cuando configuramos las interfaces ponemos **Terminado** y luego entramos en menú de *DNS* y puerta de enlace para poder conectar nuestra red con otras redes e internet.
>Configuraciones de DNS y puerta de enlace(Gateway)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_040.png)

19 . Ponemos *DNS* primario y secundario en caso de que primario no funcione y la *Gateway*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_039.png)

20 . Cuando tenemos todo configurado seleccionamos **Terminado** y luego sale el menú de configuración del servidor *DHCP*. En nuestro caso lo dejamos desactivado y seleccionamos **OK**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_041.png)

21 . Después seleccionamos **OK** y esperamos que arranque el router.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_042.png)

##Linea de comandos del IPRouter

1 . Nos introducimos como usuario **root** y con la *contraseña que hemos configurado antes.*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_043.png)

2 . Para ver o cambiar configuración del router podemos modificar el fichero de configuración con siguiente comando:

  

     vi /var/ipfire/ethernet/settings

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_044.png)

3 . Para probar si funciona la interfaz **GREEN** (red local) configuramos una o dos máquinas en la misma red *IP* y hacemos *ping* a ellas.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_047.png) 

4 . Para probar la interfaz **RED** hacemos ping al *Gateway* que hemos configurado antes y otro hasta google.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_048.png)

5 . Al final desde un ordenador de nuestra red local entramos en la página de administración del router poniendo lo siguiente en el navegador -  https://*ip del interfaz **GREEN** que es la gateway de la red local*: *y el puerto 444* en nuestro caso sería:

    https://172.16.100.1:444

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_046.png)



Instalación y configuración del servidor
================================

##*Requisitos*

- Imagen de OpenSUSE
- Procesador - pentium 4 2.4Ghz o mejor
- Memoria RAM - 1Gb o más
- Disco Duro - 5Gb o más
- Tarjeta gráfica - 800x600 display resolution
- Tarjeta de RED - 100Mbit/1Gbit

##Instalación

1 . Seleccionamos **Installation**

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_051.png)


2 . Seleccionamos el **idioma** y el **teclado**  *Español*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_052.png)

3 . Luego seleccionamos  **Instalación nueva** y **Utilizar configuración automática**

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_053.png)

4 . Configuramos región y zona horaria.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_054.png)

5 . Luego se seleciona que tipo de entorno gráfico queremos. Depende del usuario se puede seleccionar **GNOME** , **KDE** o algúno otro que se quiera. En nuestro caso seleccionamos **KDE**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_055.png)

6 . Con el siguiente menú empezamos con la configuración de las particiones.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_056.png)

7 . Para optimizar las particiones como queremos nosotros seleccionamos **Particionado personalizado**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_057.png)

8 . Primero creamos una partición **SWAP** con los siguientes pasos. *Añadir partición*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_058.png)

9 . Ponemos la partición como **primaria**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_059.png)

10 . Como **tamaño** de la particion ponemos 2Gb.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_060.png)

11 . Como **Sistema de archivos** y **Punto de montaje** ponemos **SWAP**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_061.png)

12 . Cuando tenemos la partición **SWAP** hecha empezamos con la partición **raíz**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_062.png)

13 . Como partición *primaria*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_063.png)

14 . Utilizamos el tamaño máximo que falta por seleccionar.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_065.png)

15 . Como **Sistema de archivos** ponemos *ext4* y como **Punto de montaje** ponemos */* que es la directorio raíz.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_066.png)

16 . Cuando tenemos todo configurado seleccionamos *Aceptar*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_067.png)

17 . Luego *Siguiente*. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_069.png)

18 . El paso final es crear el usuario y contraseña.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_070.png)

19 . Cuando tenemos todo configurado sale un resumen de todas las configuraciones. Si todo esta correcto seleccionamos **Instalar**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_071.png)

20 . Luego sale otra confirmación para instalar.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_072.png)

21 . Empieza la instalación.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_073.png)

22 . Cuando termina la instalación tenemos que reiniciar la maquina.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_074.png)


##Configuración de red

1 . Arrancamos el sistema.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_075.png)

2 . Para configurar la tarjeta de red utilizamos la aplicación **YaST**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_079.png)

3 . Selecionamos *Dispositivos de red* y luego a la derecha *Ajustes de red*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_080.png)

4 . Vamos a la pestaña siguiente de **Opciones Globales**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_090.png)

5 . En **Vista resumen** seleccionamos la tarjeta y luego *Editar*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_091.png)

6 . En **Dirreción** seleccionamos *Dirreción IP estática asignada* y ponemos la **IP** , **Máscara** y **Nombre de Host**.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_088.png)

7 . En la pestaña **Nombre de Host/DNS** le ponemos el nombre de *host y dominio* y como DNS primario ponemos 8.8.8.8 y 4.4.4.4 como secundario.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_092.png) 

9 . En la última pestaña **Encanamiento** es donde ponemos la *Puerta de enlace(Gateway)*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_094.png)

10 . Cuando todo esta configurado podemos probar si funciona bien abriendo un terminal y ponemos lo siguiente.

    ifconfig

Se ve que nuestra la tarjeta de red *eth0* que tiene la *IP* 172.16.100.100 y la *máscara* 255.255.255.0

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_086.png)

11 . Para verificar la conexión con otros dispositivos en red hacemos un *ping +(ip)* a ellos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/e7ee977f754918ec37d9691e4ff1fa130a317e67/proyecto%20img/Selecci%C3%B3n_089.png)


Resumen:
=========

[TOC]


 

