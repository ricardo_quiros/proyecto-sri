Instalación y configuración de *POSTFIX* y servidor *POP/IMAP*. 
==========================

**Instalación de POSTFIX**

1.Abrimos el terminal con privilegios de administrador y escribimos el siguiente comando para instalar POSTFIX:  

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_548.png)

2.Cuando lo tenemos instalado escribimos **cd /etc/postfix** para ir a la carpeta de postfix.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_549.png)

3.Con el editor de texto *vi* abrimos el fichero *main.cf*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_550.png)

4.Ahora tenemos que cambiar varias filas para configurar nuestro postfix.

 - Primero configuramos nuestro domain.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_556.png)

 - Ponemos en *mydestination y myhostname* , *$mydomain*, esta variable es *informatica.net*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_551.png)

 - Para *mynetworks* ponemos que postfix funciona solo dentro de nuestra red local que es 172.16.100.0/24.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_555.png)

 - En *inet_interfaces* ponemos a *all*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_552.png)

 - Luego configuramos la carpeta donde recibirán los correos cada usuario que tenemos, *Maildir*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_554.png)

 - Y al final ponemos un limite de 10MB para los correos que enviamos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_553.png)

5.Cuando tenemos todo configurado, guardamos el fichero y reiniciamos nuestro servidor *POSTFIX* con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_557.png)

6.Anteriormente hemos creado dos usuarios locales *mortadelo* y *filemon* donde cada usuario tiene su directorio dentro del directorio /home.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_558.png)

7.Hacemos una prueba de postfix, con el usuario root enviamos un correo a filemon:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_559.png)

8.Luego entramos dentro del directorio **/home/filemon/Maildir/new** . El directorio Maildir/  y su contenido se ha creado automáticamente después de haber enviado el correo. Observamos los mensajes nuevos.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_560.png)

9.Con el comando *cat* podemos ver el correo. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_561.png)

**Instalación de servidor POP/IMAP**

1.Utilizamos *DOVECOT* para el servidor POP/IMAP. Lo instalamos con el siguiente comando:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_562.png)

2.Cuando lo tenemos instalado entramos dentro de su directorio de configuración */etc/dovecot*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_563.png)

3.Dentro del directorio hay un fichero que se llama *dovecot.conf*, lo abrimos con *vi dovecot.conf* y configuramos que el protocolo con el que trabajaremos es *IMAP*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_565.png)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_564.png)

4.Dentro del mismo directorio accedemos a otro directorio que se llama **conf.d**.  

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_566.png)

Si escribimos el comando *ls* observamos que hay muchos ficheros de configuración.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_567.png)

5.Primero abrimos el fichero *10-master.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_568.png)

Lo configuramos para que escuche solo por el puerto 993 para IMAP y activamos SSL.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_569.png)

6.Para configurar SSL abrimos el fichero *10-ssl.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_571.png)

Ponemos *ssl = yes* y abajo ponemos las rutas de nuestro certificado digital que hemos creado antes con *opnessl*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_570.png)

7.Para configurar el modo te autenticación abrimos el fichero *10-auth.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_574.png)

Tenemos que modificar las siguientes dos líneas:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_572.png)

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_573.png)

8.Al final configuramos en que carpeta se almacenarán los correos. Con el fichero *10-mail.conf*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_576.png)

Como directorio ponemos el mismo que en *POSTFIX* **Maildir**.


![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_575.png)

Cuando tenemos todo configurado, reiniciamos *dovecot* con el siguiente comando:

    systemctl restart dovecot.service



**Instalación y configuración de clientes POP/IMAP.**

**Cliente webmail**

Para el cliente webmail vamos utilizar *SquirrelMail*. Para que funcione desde *webmail.informatica.net* tenemos que configurar el servidor *apache2*.

Cuando tenemos configurado *apache2* instalamos *SquirrelMail*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_577.png)

*SquirrelMail* se instalará dentro del directorio /srv/www/htdocs/squirremail .

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_578.png)

Para configurarlo tenemos que entrar dentro del directorio *config* y arrancamos el script *conf.pl*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_579.png)

Para arrancar el script ponemos lo siguiente:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_580.png)

Cuando lo arrancamos sale el siguiente menú:

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_582.png)

Para personalizarlo introducimos **1** e intro para entrar dentro del menú *Organization Preference*. 

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_583.png)

Luego introducimos **2** para configurarlo. Ponemos nuestro dominio *informatica.net*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_584.png)

Para configurar el puerto que se conectará con el servidor y permitir SSL introducimos **A** e intro. Aquí ponemos el puerto *993* y Secure Imap a *true*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_585.png)

Cuando tenemos todo configurado, podemos entrar en nuestro *SquirrelMail*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_586.png)

Como usuario utilizamos *mortadelo*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_587.png)

Para probar enviamos un correo a *filemon@informatica.net*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_588.png)

Luego entramos con el usuario *filemon* y aquí se ve que hemos recibido el correo de *mortadelo*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_589.png)

**Cliente de escritorio**

Para el cliente de escritorio utilizamos *Thunderbird*.  Entramos con el usuario *filemon* y lo configuramos para que utilice el puerto *993* con *SSL* y como identificación *Contraseña normal*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_590.png)

Thunderbird avisa que nuestro certificado es autofirmado y pregunta si queremos aceptarlo. Seleccionamos sí.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_591.png)

Aquí vemos nuestro certificado digital que hemos creado con *OpenSSL*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_592.png)

Para probar el funcionamiento enviamos un correo de *filemon* a *mortadelo*

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_593.png)

Aquí se ve que *mortadelo* ha recibido el correo de *filemon*.

![enter image description here](https://bytebucket.org/ricardo_quiros/proyecto-sri/raw/84257fae01fa42e8c7b043cc98f2921f25a1a159/IMG%20POSTFIX/Seleccio%CC%81n_594.png)